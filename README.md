### Hi there 👋


I am Mahaveer Rulaniya, student at IIT Kharagpur. I am proficient in Machine learning, Statistics and BI tools with the understanding of Business domain.

- 🔭 I’m currently working on Machine Learning & Software Development
- 🌱 I’m currently learning Data Structure & Algorithms
- 👯 I’m looking to collaborate on **AI and Data Science Projects**
- 💬 Ask me about Tensorflow, ML algorithms and Deep learning concepts
- 📫 How to reach me: [LinkedIn](https://www.linkedin.com/in/mahaveer-rulaniya/) or you can mail me at mahaveer.iitkgp@gmail.com
- 😄 IIT Kgpian
- ⚡ Fun fact: I sleep in my dreams also

